from django.apps import AppConfig


class SabjimanddiApiConfig(AppConfig):
    name = 'sabjimanddi_api'
